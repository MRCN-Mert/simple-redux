import React, { Component } from "react";

export default class Iletisim extends Component {
    render() {
        return (
            <section>
                <div className="iletisim">
                    <div className="container">
                        <div className="flex">
                            <div className="ck12">
                                <h2>İletişim</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}