import React, { Component } from "react";
import {connect} from 'react-redux';
import * as action1 from '../redux/actions/action1';

class Footer extends Component {
    onclick=(e)=> {
        action1.setR1Name("Mert");
    }
    render() {
        const sosyalAglar = this.props.children;
        const { yazar } = this.props;
        return (
            <footer>
                <div className="container">                    
                        {sosyalAglar}
                        <button onClick={this.onclick}>Update</button>
                        <p>{this.props.rName}</p>
                        Copyright © {new Date().getFullYear()} | {yazar}
                </div>
            </footer>
        );
    }
}

const mapStateToProps = (state, ownProps) => ({
   rName : state.reducer1.R1Name,
   rValue : state.reducer1.R1Value
});

Footer = connect(mapStateToProps)(Footer);

export default Footer;