import React, { Component } from "react";

export default class Konular extends Component {
    
    render() {
        const { konular } = this.props;
        // eslint-disable-next-line
        const basliklar2 = konular.map(e => e.baslik).join();
        const konular2 = konular.map((e, i) =>
            <div className="ck12 o6 b4" key={i}>
                <div className="konu">
                    <h4>{e.baslik.join(' ')}</h4>
                    <a href={e.link} target="_blank" rel="noreferrer noopener">TIKLA</a>
                    <span className="tarih"> {e.tarih} </span>
                    <div className="detay">
                        <p>
                            {e.aciklama}
                        </p>
                    </div>
                </div>
            </div>
        )
        return (
            <section>
                <div className="referanslar">
                    <div className="container">
                        <div className="flex">
                            <div className="ck12">
                                <h2>Konular</h2>
                                <div className="flex">
                                    { konular2 }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}