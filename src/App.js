import React, { Component, Fragment } from 'react';
import { BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import Header from './components/Header.js'; 
import SocialMedia from './components/SocialMedia.js';
import Iletisim from './components/Iletisim.js';
import Konular from './components/Konular.js';
import Footer from './components/Footer.js';
import Name from './components/Name.js';
import './App.css';

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      count : 0, 
      isim : {
        adi: "Merttttttt",
        lastName: "Yakdemir"
      },
      konular: [
        {
          baslik: ["mert","mert","yakdemir"],
          aciklama: "Örnek Konu",
          tarih: "Mayıs 2018",
          link : "https://www.google.com.tr/"
        },
        {
          baslik: ["MertYakdemir2"],
          aciklama: "Örnek Konu",
          tarih: "Mayıs 2018",
          link : "https://www.google.com.tr/"
        }
      ],
      yazar: "Mert Yakdemir"
    }
    this.mobilMenu = this.mobilMenu.bind(this);
    /* Fena olmayan kullanım şekli 
    this.inc = this.inc.bind(this);
    this.dec = this.dec.bind(this);
    */
  }
  inc=(e)=> {
    this.setState({ count : this.state.count + 1})
  } 
  dec=(e)=> {
    this.setState({ count : this.state.count - 1 })
  } 

  mobilMenu(e) {
    if (document.body.offsetWidth < 1024) {
      const header = document.getElementsByTagName("header")["0"];
      if (!header.classList.contains("active")) {
        header.classList.add("active");
      }
      else {
        header.classList.remove("active");
      }
    }
  }
  componentDidMount() {
    window.addEventListener("scroll", function(e){
      if (document.body.offsetWidth >= 1024) {
        const header = document.getElementsByTagName("header")["0"];
        let scrollPosition = window.scrollY;
        if(scrollPosition > 50) {
          header.classList.add("active");
        }
        else {
          header.classList.remove("active");
        }
      }
    });
  }
  render() {
    const { konular, yazar, isim} = this.state;
    return (
      <Router>
        <Fragment>
          <Header onClick={this.mobilMenu} />
          <main>
            <Switch>
              <Route exact path="/" render={()=> 
              <Konular 
               konular={konular}
             />}
      />
      <Route path="/iletisim" component={Iletisim} />
      </Switch>
      {/* En kötü kullanım şekli her seferinde bind ediliyor performansı düşürüyor
      <div> 
      {this.state.count} 
      <button onClick={this.inc.bind(this)}>Arttır</button>
      <button onClick={this.dec.bind(this)}>Azalt</button> 
      </div>
      */}
       {/* Fena olmayan kullanım şekli 
      <div> 
      {this.state.count} 
      <button onClick={this.inc}>Arttır</button>
      <button onClick={this.dec}>Azalt</button> 
      </div>
      */}
      {/* En iyi kullanım şekli setState'e arrow function eklemek */}
      <div> 
      {this.state.count} 
      <button onClick={this.inc}>Arttır</button>
      <button onClick={this.dec}>Azalt</button> 
      </div>
      </main>
      <Footer yazar={yazar}>
      <SocialMedia />
      </Footer> 
      <div> <Name names="merttt" /> </div>
      <div> {isim.adi} {isim.lastName}</div> 
      </Fragment>
      </Router>
    );
  }
}
