import {store} from '../../index.js';

export function setR1Name(R1Name) {
    store.dispatch({
        type: 'SET_R1Name',
        payload: {
            R1Name
        }
    });
}